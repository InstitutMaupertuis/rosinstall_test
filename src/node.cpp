#include <ros/ros.h>
#include <fanuc_post_processor/post_processor.hpp>

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "node");
  ros::NodeHandle nh;
  fanuc_post_processor::FanucPostProcessor p;
  return 0;
}
