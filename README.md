[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Compile
```bash
mkdir -p $HOME/code/catkin_workspace
cd $HOME/code/catkin_workspace
git clone https://gitlab.com/InstitutMaupertuis/rosinstall_test.git
wstool init src rosinstall_test/rosinstall_test.rosinstall &&\
mv rosinstall_test src
cd ..
```

```bash
rosdep install --from-paths src --ignore-src --rosdistro melodic -y
```

```bash
catkin_make
```
